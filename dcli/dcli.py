#!/usr/bin/env python
# encoding: utf-8
'''
dcli -- SP Deploy CLI interface

dcli is a description

It defines classes_and_methods

@author:     Iaroslav Molochko

@copyright:  2014 Oberthur Technologies. All rights reserved.

@license:    Oberthur

@contact:    i.molochko@oberthurcs.com
@deffield    updated: Updated
'''

import sys
import os
import yaml
import re
from suds.client import Client as WSClient
from suds.plugin import MessagePlugin
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

import logging
DEBUG = 1
TESTRUN = 0
PROFILE = 0

logging.basicConfig(level=logging.INFO)
if DEBUG:
    LOGLEV = 'DEBUG'
else:
    LOGLEV = 'CRITICAL'

logging.getLogger('suds.client').setLevel(getattr(logging, LOGLEV))
logging.getLogger('suds.transport').setLevel(getattr(logging, LOGLEV))


class DeleteEmptyOptional(MessagePlugin):
    def sending(self, context):
        context.envelope = re.sub('<[a-zA-Z0-9]*/>', '', context.envelope)

__all__ = []
__version__ = 0.1
__date__ = '2014-05-12'
__updated__ = '2014-05-12'

# dictionaries type <--> factory definition

create = {'CardProfile': 'createCardProfileService',
          'UsimProfile': 'createUsimProfileProvService',
          'Package': 'createApplicationPackageService',
          'Application': 'createApplicationProvService',
          'SecurityDomain': 'createSecurityDomainProvServices',
          'Midlet': 'createMidletService',
          'Partner': 'createPartnerService',
          'Service': 'createService',
          'Device': 'createDevice',
          }

update = {'Device': 'updateDeviceService',
          'Midlet': 'updateMidletService',
          'SecurityDomain': 'updateSecurityDomainProvServices',
          'Application': 'updateApplicationService',
          'PackageByAid': 'updateApplicationPackageByAidService',
          'PackageById': 'updateApplicationPackageByIdService',
          'CardProfile': 'updateCardProfileService',
          'UsimProfile': 'updateUsimProfileProvService',
          }

delete = {'Device': 'deleteDeviceByName',
          'Service': 'deleteService',
          'Partner': 'deletePartnerByName',
          'Midlet': 'deleteMidletService',
          'SecurityDomainByName': 'deleteSecurityDomainServicesByName',
          'SecurityDomainByAid': 'deleteSecurityDomainServicesByAid',
          'Application': 'deleteApplicationProvService',
          'Package': 'deletePackageService',
          'CardProfile': 'deleteCardProfileService',
          'UsimProfile': 'deleteUSIMProfileService',
          }


class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg

    def __str__(self):
        return self.msg

    def __unicode__(self):
        return self.msg


def main(argv=None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Iaroslav Molochko on %s.
  Copyright 2014 Oberthur Technologies. All rights reserved.

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument('-p', '--endpoint', dest='endpoint', metavar="URL", help="Endpoint of the SOAP service")
#         parser.add_argument("-t", "--type", dest="type", choices=set(create.keys() + update.keys() + delete.keys()),
#                             help="Entity type to work with", required=True)
        parser.add_argument('-c', '--create', dest='create', choices=create.keys(), help="Create entity")
        parser.add_argument('-u', '--update', dest='update', choices=update.keys(), help="Update entity")
        parser.add_argument('-d', '--delete', dest='delete', choices=delete.keys(), help="Delete entity")
        parser.add_argument('-i', '--input', dest="input", metavar="path", help="YAML file with variables", required=True)
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", default=False, help="set verbosity level [default: %(default)s]")
        parser.add_argument('-V', '--version', action='version', version=program_version_message)

        # Process arguments
        args = parser.parse_args()

        if args.endpoint:
            endpoint = args.endpoint
        elif os.environ.get('DEPLOY_ENDPOINT'):
            endpoint = os.environ.get('DEPLOY_ENDPOINT')
        else:
            print "Don't know where to connect to, please set either DEPLOY_ENDPOINT variable or endpoint parameter"
            sys.exit(-1)

        input = args.input
        verbose = args.verbose
        create_opt = args.create
        update_opt = args.update
        delete_opt = args.delete

        if verbose > 0:
            print("Verbose mode on")

        # make am action decision
        if create_opt:
            method = create.get(create_opt)
        elif update_opt:
            method = update.get(update_opt)
        elif delete_opt:
            method = delete.get(delete_opt)
        else:
            print "action parameter is mandatory. For help use -h or --help"
            sys.exit(-1)

        try:
            client = WSClient(endpoint, location=endpoint.replace('?wsdl', ''), plugins=[DeleteEmptyOptional()])
        except Exception, e:
            raise(e)

        req = client.factory.create(method)
        key = req.__keylist__
        if key:
            val_req = getattr(req, key[0])
        else:
            val_req = req

        # populate request with variables
        with open(input) as fd:
            for item in yaml.load(fd):
                for k, v in item.items():
                    if hasattr(val_req, k):
                        val_req[k] = v

                call = getattr(client.service, method)
                result = call(val_req)
                # print result
        # expecting synchroneus answer
        #result = client.service.process(req)

        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        if DEBUG or TESTRUN:
            raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help\n")
        return 2

if __name__ == "__main__":
    if DEBUG:
        sys.argv.append("-v")
    sys.exit(main())
